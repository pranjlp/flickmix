# Overview
Flickmix tries to use a non-data based approach to a recommendation system for movies and tv shows

# Examples
```bash
$ ./flickmix.py ls053033018 -l 0.8  --skiprecent
              0    1         2
0        Action   32  0.092754
1     Adventure   26  0.075362
2     Animation    6  0.017391
3     Biography   36  0.104348
4        Comedy   72  0.208696
5         Crime   52  0.150725
6   Documentary    1  0.002899
7         Drama  317  0.918841
8        Family   15  0.043478
9       Fantasy   21  0.060870
10    Film-Noir    3  0.008696
11      History   21  0.060870
12       Horror    8  0.023188
13        Music   20  0.057971
14      Musical   14  0.040580
15      Mystery   21  0.060870
16      Romance   68  0.197101
17       Sci-Fi    7  0.020290
18        Sport   10  0.028986
19     Thriller   49  0.142029
20          War   32  0.092754
21      Western    8  0.023188
        0   1         2
0   1920s   3  0.008696
1   1930s   4  0.011594
2   1940s  11  0.031884
3   1950s  33  0.095652
4   1960s  37  0.107246
5   1970s  37  0.107246
6   1980s  41  0.118841
7   1990s  42  0.121739
8   2000s  60  0.173913
9   2010s  73  0.211594
10  2020s   4  0.011594
Not considering  {'Drama', 'Documentary', 'Film-Noir'}
Initialised
100%|█████████████████████████████████████████████████████████████████████████████████████████████████| 345/345 [07:10<00:00,  1.25s/it]
                           Title  Year                                     URL         Runtime                     Genres
0       In the Company of Actors  2007   https://www.imdb.com/title/tt1059798/ 0 days 01:15:00                Documentary
1                Brief Encounter  1945   https://www.imdb.com/title/tt0037558/ 0 days 01:26:00             Drama, Romance
2                       Kummatty  1979   https://www.imdb.com/title/tt0079422/ 0 days 01:30:00             Drama, Fantasy
3          Zendegi va digar hich  1992   https://www.imdb.com/title/tt0105888/ 0 days 01:35:00           Adventure, Drama
4                     The Father  2020  https://www.imdb.com/title/tt10272386/ 0 days 01:37:00             Drama, Mystery
5                      Jalsaghar  1958   https://www.imdb.com/title/tt0051792/ 0 days 01:40:00               Drama, Music
6     La passion de Jeanne d'Arc  1928   https://www.imdb.com/title/tt0019254/ 0 days 01:50:00  Biography, Drama, History
7               Shor in the City  2010   https://www.imdb.com/title/tt1916728/ 0 days 01:52:00               Crime, Drama
8                     Possession  1981   https://www.imdb.com/title/tt0082933/ 0 days 02:04:00              Drama, Horror
9               Meghe Dhaka Tara  1960   https://www.imdb.com/title/tt0054073/ 0 days 02:06:00             Drama, Musical
10  Mr. Smith Goes to Washington  1939   https://www.imdb.com/title/tt0031679/ 0 days 02:09:00              Comedy, Drama
```
# F.A.Q
- **Why does this take so much time?**\
It is a slow algorithm by nature. It is possible to provide quick results with lesser choices in the result. This project just provides the longest list of non-overlapping genres and decades. I personally do not find the need for quick running script for my usecase, as I get a list that keeps me busy for a while.

- **I get too few results.**\
The list you are providing is skewed. Has a lot of movies from one genre or decade. Note its percentage, like Drama is 91% in the example list. Pass a threshold via -l switch so that the skewed genre is not considered.

- **Why some other genres are skipped?**\
Some genres or decades are skipped, if they have too less (< 1%) of contribution in the given list.

- **How do I find the imdb id of my watchlist**\
Go to your [watchlist](https://www.imdb.com/list/watchlist). Scroll to the bottom where there is a link to Export the list. The link has the imdb list id of your watchlist  


# Build and Usage
Requires python to be installed. It is recommended to be installed in a python virtual environment.
```python
pip install -r requirements.txt
```
```bash
usage: flickmix [-h] [-t {tvmini,movie,tv}] [-s] [-l THRESHOLD]
                [-z {low_runtime,high_rating}]
                list

shortlists a mix of movies from a sufficiently long watchlist

positional arguments:
  list                  the imdb list id in the format lsXXXXXXXXX

options:
  -h, --help            show this help message and exit
  -t {tvmini,movie,tv}, --type {tvmini,movie,tv}
                        the type of title to consider for
                        shortlisting, tvmini is for tvshows with
                        less than 30 min episode runtime, not to be
                        confused with mini series, which are
                        included in tv
  -s, --skiprecent      skip titles released in last two years
  -l THRESHOLD, --threshold THRESHOLD
                        skip genres or decades that occur too
                        often in the list, takes a value between 0
                        and 1. 1 means if all titles are of the
                        same genre or decade, it won't be
                        considered for calculating the mix, useful
                        for lists targeting a specific genre or
                        decade
  -z {low_runtime,high_rating}, --optimize {low_runtime,high_rating}
                        the mix is optimised for high ratings or
                        lower runtimes. Note: the primary
                        optimisation is to maximise the number of
                        results
```

# ToDo
- Seperate the imdb parsing logic from selection algorithm and rewrite in a language easy to cross-compile
- Add support for shorts and video games

# Story
Countless arguments, friends and foes, have, when deciding what to watch, in a gathering. *Not another comedy*, *Had enough of the 90s BS*, *Its been a while we saw a sports movie*. This sounds very similar to the independent set problem in computer science, which inspired this. 
