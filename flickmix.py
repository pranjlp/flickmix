#!/usr/bin/env python
import copy
import argparse
from datetime import datetime, timedelta
import pandas as pd
from pandas.core.frame import DataFrame as DF 
from typing import cast
from itertools import chain, combinations
from tqdm import tqdm
from imdb import Cinemagoer
import urllib, json
from math import log, floor
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import partial
#from sqlite3 import Connection, Cursor, connect, threadsafety
selected = []

def get_episode_count(title: str):
    base_url = f"https://api.tvmaze.com/lookup/shows"
    params = {"imdb": title}

    # Construct the URL
    url = f"{base_url}?{urllib.parse.urlencode(params)}"

    # Make a GET request using urllib
    with urllib.request.urlopen(url) as response1:
        data1 = json.loads(response1.read().decode('utf-8'))
        id = data1["id"]
        show_info_url = "https://api.tvmaze.com/shows/{}/episodes".format(id)
        print(show_info_url)
        with urllib.request.urlopen(show_info_url) as response:
            data = json.loads(response.read().decode('utf-8'))
            return len(data) 

def add_episode_metadata(all_movies: DF):
    cached_movies = cast(DF, pd.read_csv('episode_data'))
    not_found = list(set(all_movies['Const']) - set(cached_movies['Const']))
    print('Episode count not cached for: %s titles' % len(not_found))
    if not_found:
        with ThreadPoolExecutor() as pool:
            found_futures = { pool.submit(get_episode_count, title):title for title in not_found }
            found_dict = {}
            for future in as_completed(found_futures):
                title = found_futures[future]
                try:
                    found_dict[title] = future.result()
                except Exception as ex:
                    print("Fetching episode_count bugged for", title, ex)
            found_movies = DF(found_dict.items(), columns=['Const', 'Episodes'])
        cached_movies = pd.concat([cached_movies, found_movies])
        cached_movies.to_csv('episode_data')
    all_movies = all_movies.merge(right=cached_movies, on='Const')
    
    all_movies['Episode Runtime'] = all_movies.apply(lambda x: x['Runtime'] if x['Runtime'] < timedelta(minutes=115)  else x['Runtime'] / x['Episodes'], axis=1)
    all_movies['Runtime'] = all_movies.apply(lambda x: x['Runtime'] * x['Episodes'] if x['Runtime'] < timedelta(minutes=115) else x['Runtime'], axis=1)
    return all_movies
    
def genre_stats(all_movies: DF, threshold):
    saturated_genres = set()
    all_genres = set()
    all_decades = set()
    decade_stats = dict()
    genre_stats = dict()
    total = 0
    max_year = all_movies['Year'].max()
    for i in all_movies.index:
        movie_genres = []
        try:
            movie_genres = [g.strip() for g in all_movies['Genres'][i].split(',')]
        except:
            print('Error in fetching genre for: ' + all_movies['Const'][i])
        decade = max_year - (1 << floor(log(1 + max_year - all_movies['Year'][i], 2))) + 1
        for g in movie_genres:
            if g.strip() in genre_stats:
                genre_stats[g.strip()] += 1 
            else:
                genre_stats[g.strip()] = 1
            all_genres.add(g)
        total += 1
        if decade in decade_stats:
            decade_stats[decade] += 1 
        else:
            decade_stats[decade] = 1
        all_decades.add(decade)
    saturated_genres = set(g for g in genre_stats if genre_stats[g] / total >= threshold or genre_stats[g] * 100 // total < 1)
    total_decades = sum(2 ** (x / total) for x in decade_stats.values())
    print(total_decades)
    saturated_decades = set(d for d in decade_stats if (2 ** (decade_stats[d] / total)) / total_decades >= threshold or (2 ** (decade_stats[d] / total)) * 100 // total_decades < 1)
    print(DF(sorted([(g, genre_stats[g], genre_stats[g] / total) for g in genre_stats], key=lambda x: x[0])))
    print(DF(sorted([(d, decade_stats[d], (2 ** (decade_stats[d] / total)) / total_decades) for d in decade_stats], key=lambda x: x[0])))
    all_genres -= saturated_genres
    return saturated_genres, all_genres, saturated_decades, min(len(all_genres), len(all_decades))

def genre_cover(all_movies: DF, skip_genres: set, skip_decades: set,  genre_set: set, exit_len: int):
    if len(skip_genres) > 0:
        genre_set.add('Skipped')
    genre_set = sorted(genre_set)
    genre_set = {x : (1 << k) for k, x in enumerate(genre_set)}
    max_year = all_movies['Year'].max()
    dp = {}
    for x in range((1<< len(genre_set))):
        dp[x] = [[], set(), timedelta(minutes=0)]
    print('Initialised')
    for k in tqdm(range(len(all_movies.index))):
        i = all_movies.index[k]
        movie_genres = set()
        movie_genres = all_movies['Genres'][i].split(',')
        movie_genres = set(g.strip() for g in movie_genres if g.strip() not in skip_genres)
        if all_movies['Const'][i] == 'tt4295140':
            print(movie_genres)
        if len(movie_genres) == 0:
            movie_genres.add('Skipped')
        movie_decades = max_year - (1 << floor(log(1 + max_year - all_movies['Year'][i], 2))) + 1
        movie_decades = 'Skipped' if movie_decades in skip_decades else movie_decades
        movie_runtime = all_movies['Runtime'][i]
        left_genres = set(genre_set.keys()) - movie_genres
        movie_id = sum(genre_set[x] for x in movie_genres) 
        pow_set = list(chain.from_iterable(combinations(left_genres, r) for r in range(len(left_genres)+1)))
        global selected
        for x in pow_set:
            id = sum(genre_set[a] for a in x)
            if len(dp[id + movie_id][0]) < len(dp[id][0]) + 1 or (len(dp[id + movie_id][0]) == len(dp[id][0]) + 1 and dp[id + movie_id][2] > dp[id][2] + movie_runtime):
                if movie_decades not in dp[id][1]:
                    dp[id + movie_id] = copy.deepcopy(dp[id])
                    dp[id + movie_id][0].append(i)
                    dp[id + movie_id][1].add(movie_decades)
                    dp[id + movie_id][2] += movie_runtime
                    if not selected or len(selected[0]) < len(dp[id + movie_id][0]) or (len(selected[0]) == len(dp[id + movie_id][0]) and selected[2] > dp[id + movie_id][2]):
                        selected = copy.deepcopy(dp[id + movie_id])
                    if len(selected[0]) == exit_len:
                        break
            if len(selected[0]) == exit_len:
                break


def main(args):
    r = cast(DF, pd.read_csv('https://www.imdb.com/list/' + args.list + '/export')) #ls053033018 
    r['Runtime'] = r['Runtime (mins)'].apply(lambda x: pd.to_timedelta(x,'m'))
    r.drop('Runtime (mins)', axis=1, inplace=True)
    if args.type == 'movie':
        r = r[r['Title Type'] == 'movie']
    elif args.type == 'tv':
        r = add_episode_metadata(r[((r['Title Type'] == 'tvSeries') | (r['Title Type'] == 'tvMiniSeries')) ])
        r = r[(r['Episode Runtime'] > (timedelta(minutes=30)))]
    else:
        r = add_episode_metadata(r[((r['Title Type'] == 'tvSeries') | (r['Title Type'] == 'tvMiniSeries')) ])
        r = r[(r['Episode Runtime'] <= (timedelta(minutes=30)))]
    if(args.optimize == 'high_rating'):
        r.sort_values(by='IMDb Rating', inplace=True, ascending=False)
    else:
        r.sort_values(by='Runtime', inplace=True)
    if args.skiprecent:
        r = r[r['Year'] < (datetime.now().year - 1)]

    #print(r.dtypes)
    skip_genres, genre_set, skip_decades, exit_len = genre_stats(r, args.threshold)
    print('Not considering ', skip_genres)
    try:
        genre_cover(r, skip_genres, skip_decades, genre_set, exit_len)
    finally:
        global selected
        selected = selected[0]
        selected = [r['Const'][x] for x in selected]
        selected = DF({'Const':selected})
        selected = r.merge(on='Const', right=selected)
        print(selected[['Title', 'Year', 'URL' , 'Runtime', 'Genres']])

if __name__ == '__main__':
    ap = argparse.ArgumentParser(prog='flickmix', description='shortlists a mix of movies from a sufficiently long watchlist') 
    ap.add_argument('list', help='the imdb list id in the format lsXXXXXXXXX')
    ap.add_argument('-t', '--type', default='movie', choices=['tvmini', 'movie', 'tv'], help='the type of title to consider for shortlisting, tvmini is for tvshows with less than avg episode runtime, not to be confused with mini series, which are included in tv')
    ap.add_argument('-s','--skiprecent', action='store_true', help='skip titles released in last two years') 
    ap.set_defaults(skiprecent=False)
    ap.add_argument('-l', '--threshold', type=float, default=1.0, help='skip genres or decades that occur too often in the list, takes a value between 0 and 1. 1 means if all titles are of the same genre or decade, it won\'t be considered for calculating the mix, useful for lists targeting a specific genre or decade')
    ap.add_argument('-z', '--optimize', type=str, default='low_runtime', choices=['low_runtime', 'high_rating'], help='the mix is optimised for high ratings or lower runtimes.\n Note: the primary optimisation is to maximise the number of results')
    main(ap.parse_args())
